#!/usr/bin/python2

from pyimagesearch.transform import four_point_transform
from pyimagesearch import imutils
from skimage.filters import threshold_adaptive
import numpy as np
import argparse
import cv2
import time

screenCnt = None
cap = cv2.VideoCapture(0)
cap.set(3, 1280)
cap.set(4, 1024)

while(True):
    ret, image = cap.read()
    #image = cv2.imread('./images/whiteboard.jpg')

    orig = image.copy()
    out = image.copy()

    if screenCnt is None:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(gray, 4, 30)

        (_, cnts, _) = cv2.findContours(
            edged.copy(),
            cv2.RETR_LIST,
            cv2.CHAIN_APPROX_SIMPLE
        )

        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]

        for c in cnts:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)

            if len(approx) == 4:
                screenCnt = approx
                break

    if screenCnt is None:
        continue

    if screenCnt is not None:
        screenCnt = screenCnt.reshape(4, 2)
        out = four_point_transform(orig, screenCnt)
        out = imutils.rotate(out, 180)

    cv2.imshow('Camera', orig)

    # cv2.namedWindow('Whiteboard', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('Whiteboard', 1600, 900)
    cv2.imshow('Whiteboard', out)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
